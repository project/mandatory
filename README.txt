Mandatory is an installation profile for Drupal 8, allowing Drupal to be
installed without ANY optional components (modules, themes). So you'll
end up with an initial Drupal installation where only mandatory components
are enabled.
